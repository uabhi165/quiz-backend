
const axios = require('axios').default;
// const express = require("express");
// const cors = require('cors');
// const app = express();
// app.use(cors())
const key = document.getElementById('key')

const questionIds = []

axios.get('http://localhost:3000/questions')
    .then(resp => {
        console.log(resp.data)
        const button = document.getElementById('hidden')

        const quizpage = document.getElementById("quizpage")
        quizpage.classList.add("quizpage")


        const sample = document.getElementById("sample")
        const mainDiv = document.createElement("div")

        sample.classList.add("main")



        // sample.append(mainDiv)
        const length = resp.data.length


        const formDiv = document.createElement("div")

        for (let i = 0; i < length; i++) {
            questionIds.push(resp.data[i]._id)
            // console.log(questionIds)
            const questionDiv = document.createElement("div")

            const quesDiv = document.createElement("div")
            quesDiv.innerHTML = resp.data[i].question;
            quesDiv.classList.add("quesstyle")
            questionDiv.append(quesDiv)


            questionDiv.classList.add("quesDivstyle")


            const choice = resp.data[i].options;
            const optDiv = document.createElement("div")
            optDiv.classList.add("optionstyle")

            for (let j = 0; j <= 3; j++) {
                const innerDiv = document.createElement("div")
                innerDiv.innerHTML = `<input type="radio" id="${resp.data[i]._id + j}" name="${resp.data[i]._id}" value="${choice[j]}">
                <label for="${resp.data[i]._id + j}">${choice[j]}</label> `

                optDiv.append(innerDiv)

            }


            const clearbutton = document.createElement("input")
            clearbutton.type = "button"
            clearbutton.id = resp.data[i]._id
            clearbutton.value = "clear choice"
            clearbutton.classList.add("clearbutton")

            clearbutton.addEventListener(`click`, (e) => {
                e.preventDefault()
                document.querySelector(`input[name="${clearbutton.id}"]:checked`).checked = false;
            })


            questionDiv.append(optDiv)
            questionDiv.append(clearbutton)
            formDiv.append(questionDiv)
            // mainDiv.append(formDiv)
            sample.append(formDiv)
            // quizpage.append(sample)

        }

        // button.classList.add("sumitbtn")
        button.classList.remove('hidden')

    })
    .catch(err => {
        // Handle Error Here
        console.log(err);
    });

    // console.log(questionIds)



const form = document.getElementById('form')
form.addEventListener('submit', (e) => {
    e.preventDefault()
   
    let empty = false;
    const attempt = questionIds.map(qId => 
        {
        
        return {
            question: qId,
            selections: form[qId].value ==='' ? null: form[qId].value
        }
       
    })
    
    
    const payload={
        questions:attempt
    }
    const check=attempt.filter(q=>q.selections!==null)
  
    if((check.length)==0){
       
        alert("attempt atleast one question")
       
    }
     else{
        key.classList.remove("bottom")
        
    axios.post("http://localhost:3000/attempt",payload)
    .then((data)=>{
        console.log(data)
    })
    .catch((err)=>{
        console.log(err)
    })

     }   
    
     console.log(check)
    console.log(attempt)
    
})
